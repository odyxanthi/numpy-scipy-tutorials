import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
import math
import scipy.optimize as sciopt



def CreatePath(verts):
    vertexCount = len(verts)
    if vertexCount<2:
        raise Exception('cannot create path from less than 2 points')
    codes = list()
    codes.append(Path.MOVETO)
    for i in range(1,vertexCount):
        codes.append(Path.LINETO)
    
    
def pathToXs(verts):
    xs = list()
    for x,y in verts:
        xs.append(x)
        xs.append(y)
    return xs

def xsToPath(xs):
    verts = list()
    for i in range(0,len(xs),2):
        verts.append((xs[i],xs[i+1]))
    return verts

def inequalityConstraints(xrefs, maxDelta):
    # maxDelta >= abs(x-xref) => 
    return [{'type':'ineq', 'fun' : lambda x: maxDelta-abs(x-xref)} for xref in xrefs]
  
    
def length(xs):
    sum = 0.0
    n = len(xs)
    for i in range(2,n,2):
        # [x1,y1,x2,y2,x3,y3...]
        x1 = xs[i-2]
        y1 = xs[i-1]
        x2 = xs[i]
        y2 = xs[i+1]
        dx = x2-x1
        dy = y2-y1
        sum += math.sqrt(dx*dx+dy*dy)
    return sum
    
def lengthNegative(xs):
    return -length(xs)
    

def main():    

    initialPts = [(0,0), (1,0), (1,1), (2,1),(2,2), (4,1)]
    xref = pathToXs(initialPts)
    e = 0.1
    bnds = [(x-e,x+e) for x in xref]
    res = sciopt.minimize(fun=length, x0=xref, bounds=bnds)       
    finalPts = xsToPath(res.x)
    print(res)
    
    resNeg = sciopt.minimize(fun=lengthNegative, x0=xref, bounds=bnds)       
    finalPtsNeg = xsToPath(resNeg.x)
    print(resNeg)
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for verts in [initialPts, finalPts, finalPtsNeg]:
        codes = CreatePath(verts)
        path = Path(verts, codes)
        patch = patches.PathPatch(path, facecolor='none', lw=2)
        ax.add_patch(patch)
    ax.set_xlim(-5,5)
    ax.set_ylim(-5,5)
    plt.show()
    
    
    
main()    
    
    



        
    


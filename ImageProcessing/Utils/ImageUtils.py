import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import math


def ConvertToGreyScale(inputImg):
    shape = inputImg.shape
    outputImg = np.zeros((shape[0], shape[1]))

    for x in range(shape[0]):
        for y in range(shape[1]):
            v = inputImg[x][y]
            outputImg[x][y] = int(0.21*v[0] + 0.72*v[1] + 0.07*v[2])    #luminosity formula for conversion from RGB to greyscale

    return outputImg

def FilterColorComponent(inputImg, colorIndex):
    shape = inputImg.shape
    outputImg = np.zeros(shape)

    colorFilters = [1, 1, 1]
    colorFilters[colorIndex] = 0

    for x in range(shape[0]):
        for y in range(shape[1]):
            v = inputImg[x][y]
            outputImg[x][y] = [v[0]*colorFilters[0], v[1]*colorFilters[1], v[2]*colorFilters[2]]
    return outputImg

def QuantizeGreyscaleImage(inputImg, bucketCount=16):
    shape = inputImg.shape
    outputImg = np.zeros(shape)
    denominator = int(math.floor(256/bucketCount))
    for x in range(shape[0]):
        for y in range(shape[1]):
            outputImg[x][y] = math.floor(inputImg[x][y]/denominator)*denominator
    return outputImg

def BlockAverageReplacement(inputImg, blockSize_=3):
    blockSize = int(blockSize_)
    shape = inputImg.shape
    outputImg = np.zeros(shape)

    blocksPerRow = int(math.ceil(shape[0]/float(blockSize)))
    blocksPerCol = int(math.ceil(shape[1]/float(blockSize)))

    for rowIndex in range(blocksPerRow):
        for colIndex in range(blocksPerCol):
            startRow, endRow = rowIndex*blockSize, (rowIndex+1)*blockSize
            startCol, endCol = colIndex*blockSize, (colIndex+1)*blockSize
            block = inputImg[startRow:endRow, startCol:endCol]

            elementCount = block.shape[0] * block.shape[1]
            avgColor = sum(sum(block)) / elementCount
            outputImg[startRow:endRow, startCol:endCol] = avgColor
    return outputImg

def InvertImage(img):
    img[:, :] = 255 - img[:, :]
    return img

def AddNoiseToImage(img, noiseFunc, createNewImg=False):
    if createNewImg:
        outputImg = np.array(img)
    else:
        outputImg = img

    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            outputImg[x, y] = noiseFunc(img[x, y])
    return outputImg

def GetHistogram(inputImg):
    histogram = [0*x for x in range(256)]
    for x in range(inputImg.shape[0]):
        for y in range(inputImg.shape[1]):
            pixValue = inputImg[x,y]
            histogram[int(pixValue)] += 1
    return histogram


def main():
    pass


if __name__ == '__main__':
    main()
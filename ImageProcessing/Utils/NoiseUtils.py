import numpy as np
import math
import random as rand

def SaltAndPepperNoise(originalValue, noiseProbability, salt=255, pepper=0 ):
    outputValue = 0
    saltProbability = 0.5 * noiseProbability

    randomVal = rand.random()
    if randomVal < noiseProbability:
        if randomVal < saltProbability:
            outputValue = salt
        else:
            outputValue = pepper
    else:
        outputValue = originalValue

    return outputValue

def NoiseGenerator_saltAndPepper(noiseProbability, salt=255, pepper=0):
    noiseFunc = lambda pixelValue: SaltAndPepperNoise(pixelValue, noiseProbability, salt, pepper)
    return noiseFunc

def NoiseGenerator_gaussian(sigma):
    noiseFunc = lambda pixelValue: pixelValue + rand.gauss(0, sigma)    #noise with mean value 0 and variance sigma.
    return noiseFunc


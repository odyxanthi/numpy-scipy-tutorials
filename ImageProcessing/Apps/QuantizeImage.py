import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/..')

import argparse
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import math
import time

from Utils import ImageUtils


def main():

    parser = argparse.ArgumentParser(usage='__doc__')
    parser.add_argument("--filename", default='../Data/Lena.jpg', help="image to quantize")
    parser.add_argument("--colors", type=int, default=16, help="number of color values of the quantized image")
    args = parser.parse_args()

    print('filename: ', args.filename)
    img = mpimg.imread(args.filename)

    grey = ImageUtils.ConvertToGreyScale(img)
    plotWindow = plt.imshow(grey, cmap=cm.Greys_r)

    plt.ion()
    plt.show()

    buckets = list()
    buckets.extend([2**x for x in range(0, 7)])
    buckets.extend([2**x for x in range(5, -1, -1)])

    for b in buckets:
        print('buckets = ', b)
        quantized = ImageUtils.QuantizeGreyscaleImage(grey, int(b))

        plotWindow.set_data(quantized)
        plt.draw()
        plt.pause(0.001)
        time.sleep(1.0)

if __name__=='__main__':
    main()

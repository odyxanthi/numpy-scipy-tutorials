import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/..')

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy import fftpack
from enum import Enum
import time

from Utils import ImageUtils


class PlotType(Enum):
    GRAY = 1
    SPECTRUM = 2

def updateFigure(im,title,plotType=PlotType.GRAY):
    plt.clf()
    # A logarithmic colormap
    if plotType is PlotType.SPECTRUM:
        plt.imshow(np.abs(im), norm=LogNorm(vmin=5))
        plt.colorbar()
    elif plotType is PlotType.GRAY:
        plt.imshow(im, plt.cm.gray)
    plt.title(title)
    plt.pause(0.001)
    
def doFFT(filename):  
    #set up plotting
    plt.ion()
    plt.show()
    plt.figure()

    # read image and plot original image
    im = plt.imread(filename)
    im = ImageUtils.ConvertToGreyScale(im).astype(float)
    updateFigure(im, 'Original image')
    time.sleep(1.5)
    #input('plotting original image; press enter to continue')

    # calc and plot FFT
    im_fft = fftpack.fft2(im)
    updateFigure(im_fft, 'Fourier Transform', PlotType.SPECTRUM)
    time.sleep(1.5)

    # filter 
    keep_fraction = 0.25
    im_fft2 = im_fft.copy()
    r, c = im_fft2.shape
    # Set to zero all rows with indices between r*keep_fraction and r*(1-keep_fraction):
    #im_fft2[int(r*keep_fraction):int(r*(1-keep_fraction))] = 0
    im_fft2[int(r*keep_fraction):r-int(r*keep_fraction)] = 0
    im_fft2[:, int(c*keep_fraction):c-int(c*keep_fraction)] = 0
    updateFigure(im_fft2, 'Filtered Fourier Transform', PlotType.SPECTRUM)
    time.sleep(1.5)

    # reconstruct
    im_new = fftpack.ifft2(im_fft2).real
    updateFigure(im_new, 'Reconstructed image')
    input('plotting reconstructed image; press enter to continue')


def main():
    if len(sys.argv)<2:
        print('Usage: python imageFFT.py <filename>')
        return
    
    filename = sys.argv[1]
    if os.path.isfile(filename):
        doFFT(filename)
    else:
        print('Invalid path or filename: ',path)

main()
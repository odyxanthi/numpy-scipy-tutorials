from math import *
from matplotlib import pyplot as plt
print(plt.get_backend())
import time

def main():
    plt.ion()
    plt.show()
    x = range(-50, 51, 1)
    for pow in range(0,5):
        y = [Xi**pow for Xi in x]
        plt.plot(x, y)
        plt.draw()
        plt.pause(0.001)
        time.sleep(1.500)

if __name__ == '__main__':
    main()

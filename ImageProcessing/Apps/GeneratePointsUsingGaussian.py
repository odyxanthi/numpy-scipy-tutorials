__author__ = 'odysseas'


"""example.py

Compute the maximum of a Bessel function and plot it.

"""
import argparse

import random
import numpy as np
from scipy import special, optimize
import matplotlib.pyplot as plt


def CreateRandomVec2D(intervX, intervY):
    xProportion = random.random()
    yProportion = random.random()

    var2D = (intervX[0] + (intervX[1] - intervX[0])*xProportion, intervY[0] + (intervY[1] - intervY[0])*yProportion)
    return var2D


def Uniform2DGenerator(intervX, intervY):
    f = lambda : CreateRandomVec2D(intervX,intervY)
    return f



def CreateVec2D_gaussian(meanX, sigmaX, meanY, sigmaY):
    xcoord = random.gauss(meanX,sigmaX)
    ycoord = random.gauss(meanY,sigmaY)
    return (xcoord, ycoord)


def Gaussian2DGenerator(meanX, sigmaX, meanY, sigmaY):
    f = lambda: CreateVec2D_gaussian(meanX, sigmaX, meanY, sigmaY)
    return f




def main():

    # # Parse command-line arguments
    # parser = argparse.ArgumentParser(usage=__doc__)
    # parser.add_argument("--order", type=int, default=3, help="order of Bessel function")
    # parser.add_argument("--output", default="plot.png", help="output image file")
    # args = parser.parse_args()

    #f = Uniform2DGenerator( (-10,10), (20,30))
    f = Gaussian2DGenerator(10, 22, -2, 77)

    points2D = [f() for x in range(1000)]
    xCoords = [t[0] for t in points2D]
    yCoords = [t[1] for t in points2D]


    plt.plot(xCoords, yCoords, marker='x', linestyle='None')
    plt.show()




if __name__ == "__main__":
    main()
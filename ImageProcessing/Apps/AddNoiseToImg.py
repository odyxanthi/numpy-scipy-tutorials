import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/..')

import argparse
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import math
import time

from Utils import ImageUtils
from Utils import NoiseUtils

def AddNoiseToImage(img, noiseFuncs):
    plt.ion()
    plt.show()
    plotWindow = plt.imshow(img, cmap=cm.Greys_r)
    for noiseFunc in noiseFuncs:
        noisy = ImageUtils.AddNoiseToImage(img, noiseFunc, True)
        plotWindow.set_data(noisy)
        plt.draw()
        plt.pause(0.001)
        time.sleep(1.5)
        #plt.imsave('noisy.jpg',noisy, cmap=cm.Greys_r)


def main():
    if len(sys.argv) < 2:
        print('usage: python AddNoiseToImg.py <filename>')
        return
    filename = sys.argv[1]
    print ('filename: ', filename)
    img = ImageUtils.ConvertToGreyScale(mpimg.imread(filename))

    print('adding salt and pepper noise to image')
    AddNoiseToImage(img, [NoiseUtils.NoiseGenerator_saltAndPepper(noiseProb) for noiseProb in [0.1, 0.2, 0.3, 0.4]])

    print('adding gaussian noise to image')
    AddNoiseToImage(img, [NoiseUtils.NoiseGenerator_gaussian(sigma) for sigma in np.arange(10.0,50.0,10.0)])
    

if __name__ == '__main__':
    main()

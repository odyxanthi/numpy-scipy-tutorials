import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/..')

import argparse
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import math

from Utils import ImageUtils
import time


def main():
    parser = argparse.ArgumentParser(usage='__doc__')
    parser.add_argument("--filename", default='../Data/Lena.jpg', help="image to quantize")
    parser.add_argument("--colors", type=int, default=16, help="number of color values of the quantized image")
    args = parser.parse_args()

    print('filename: ', args.filename)
    img = mpimg.imread(args.filename)
    grey = ImageUtils.ConvertToGreyScale(img)
    plotWindow = plt.imshow(grey, cmap=cm.Greys_r)

    plt.ion()
    plt.show()

    buckets = list()
    buckets.extend([2**x for x in range(7, -1, -1)])
    for b in buckets:
        print('buckets = ', b)
        resampled = ImageUtils.BlockAverageReplacement(grey, b)

        plotWindow.set_data(resampled)
        plt.draw()
        plt.show(block=False)
        plt.pause(0.001)
        time.sleep(1.5)

if __name__=='__main__':
    main()
